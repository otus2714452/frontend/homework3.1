internal class Program
{
    private static void Main(string[] args)
    {
        string Origin = "MyAllowSpecificOrigins";

        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: Origin,
                              corsBuilder =>
                              {
                                  corsBuilder.WithOrigins(builder.Configuration.GetSection("CORS:Origins").Get<string[]>())
                                   .WithHeaders(builder.Configuration.GetSection("CORS:Headers").Get<string[]>())
                                   .WithMethods(builder.Configuration.GetSection("CORS:Methods").Get<string[]>());
                              });
        });

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var app = builder.Build();

        app.UseDefaultFiles();
        app.UseStaticFiles();

        app.UseCors(Origin);

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();

        app.MapControllers();

        app.MapFallbackToFile("/index.html");

        app.Run();
    }
}